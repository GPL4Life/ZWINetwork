#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#
# Share ZWI files on a local file system.
# It also updates the mirror in real time.
# Check all options as:  python3 zwi_share.py -h 
# Note all times are in UTC. 
# Testing: 
# http://69.197.158.246/site1/ZWI/
# http://69.197.158.246/site2/ZWI/
# http://69.197.158.246/site3/ZWI/
# http://69.197.158.246/site4/ZWI/
# http://69.197.158.246/site5/ZWI/

#
# Changelog:
# ---------
# version 1.1, Dec 24, 2022: Update functionality
# version 1.0, Nov 30, 2022: Fist draft to show the concept. 
# = S.Chekanov (KSF) = 
#

import requests, math
import shutil, argparse 
import signal, time, glob, re
import os, time, sys, gzip, os.path

from requests.adapters import HTTPAdapter
from datetime import datetime

# import all modules from this directory 
script_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(script_dir)
from make_index import *

#### input parameters ####
kwargs = {}
parser = argparse.ArgumentParser()
parser.add_argument("-q", "--quiet", action="store_true", help="don't show verbose")
parser.add_argument("-o", "--output", help="Save output to the specified directory")
parser.add_argument("-i", "--input", help="Input URL of the existing ZWI database")
parser.add_argument("-s", "--summary", action="store_true", help="Prints the summary of the remote network files and exit.")
parser.add_argument("-p", "--publisher", help="Select publishers matching a pattern. Use ',' for separation. Example: 'wikipedia,citizendioum,handwiki'")
parser.add_argument("-e", "--exclude", help="Exclude publishers matching a pattern. Use ',' for separation. Example: 'wikipedia,citizendioum,handwiki'")
parser.add_argument("-l", "--local", action="store_true", help="Prints the summary of local directory when using the option \"-s\". Use \"-o\" to point to the local directory. ")
parser.add_argument("-m", "--monitoring", action="store_true", help="Monitoring all available sites and accumulate results")

args = parser.parse_args()
isVerbose = not args.quiet

if len(sys.argv) == 1:
    parser.print_help(sys.stderr)
    sys.exit(1)

if not args.local:
   print(f"Input URL = {args.input}")

excluded = []
if args.exclude:
  excluded = args.exclude.split(",")
  print(f"Excluded publishers: {', '.join(excluded)}")

publishers = []
if args.publisher:
  publishers = args.publisher.split(",")
  print(f"Included publishers: {', '.join(publishers)}")

print(f"Is quiet = {args.quiet}")

# default time monitoring (in min)
TimeDelayMin = 0.5; # 1/2 of a minute (30 seconds)  

# active URL sites
activeSites = []

# default URL
# root_repo="https://encycloreader.org/db/ZWI/"

# debug
root_repo = "http://69.197.158.246/site1/ZWI/" 

if args.input and len(args.input) > 0:
    root_repo = args.input

# output directory
output = "ZWI"
if args.output and len(args.output) > 0:
    output = args.output 

# create a session oject
session = requests.Session()
session.mount("http://", HTTPAdapter(max_retries=5))
session.mount("https://", HTTPAdapter(max_retries=5))

def getter(
    url: str,
    dest: str
    ) -> bool:

    try:

        # download the content of the file
        downloaded = False
        for _ in range(5): # retry limit
            try:
                with session.get(url, stream=True, timeout=(30, 300)) as resp:
                    with open(dest, 'wb') as fd:
                        shutil.copyfileobj(resp.raw, fd)

                downloaded = True
                break

            except Exception:
                time.sleep(0.5)
                continue

        if not downloaded:
            return False

        return True
    
    except Exception:
        return False

################## catch Ctrl+C ###############
def signal_handler(sig, frame):
    global excluded, output, root_repo 

    """ action when pressing Ctrl+C """
    print('\nPressed [Ctrl]+[C] to interrupt the program')
    print('Save the current status and exit')

    createIndexFiles(
        output, 
        excluded, 
        verbose=False
    )

    copyAuxilary(
        output, 
        root_repo,
        verbose=False
    )

    session.close()

    sys.exit(0)

# handle exit
signal.signal(
    signal.SIGINT, 
    signal_handler
)
# signal.pause()

root_repo = root_repo.strip()
output = output.strip()
if not root_repo.endswith("/"): # add trailing slash
    root_repo += "/"

if not args.local:
    print(f"Main site URL    = {root_repo}")
    print(f"Output directory = {output}")
    print(f"Start at {datetime.now()} (local time)")

# main download area
main_repo = f"{root_repo}en/"
root_repo_default = root_repo
main_repo_default = main_repo

# just check that everything is fine:
r = session.head(f"{root_repo}en/index.csv.gz")
if not r.status_code == requests.codes.ok:
   print("")
   print("ERROR")
   print("Cannot find index.csv.gz in this URL")
   print("It is possible you do not give correct location to ZWI database. Exit!")
   sys.exit(1)

# Colored printing functions for strings that use universal ANSI escape sequences.
# fail: bold red, pass: bold green, warn: bold yellow,
# info: bold blue, bold: bold white
@staticmethod
def print_fail(message, end = '\n'):
    print(f'\x1b[1;31m{message.strip()}\x1b[0m{end}')

def print_pass(message, end = '\n'):
    print(f'\x1b[1;32m{message.strip()}\x1b[0m{end}')

def print_warn(message, end = '\n'):
    print(f'\x1b[1;33m{message.strip()}\x1b[0m{end}')

def print_info(message, end = '\n'):
    print(f'\x1b[1;34m{message.strip()}\x1b[0m{end}')

def print_bold(message, end = '\n'):
    print(f'\x1b[1;37m{message.strip()}\x1b[0m{end}')

##############################################
# Compare 2 dictionaries d1 (new) with d2 (old)
##############################################
def dict_compare(
    d1: dict,
    d2: dict
    ) -> tuple[set, set, dict, set]:

    # compare 2 dictionaries. d1 is new dictionary, d2 is old dictionary  
    d1_keys = set(d1.keys())
    d2_keys = set(d2.keys())

    shared_keys = d1_keys.intersection(d2_keys)
    added = d1_keys - d2_keys
    removed = d2_keys - d1_keys

    modified = {
        o: (d1[o], d2[o]) 
        for o in shared_keys
        if d1[o] != d2[o]
    }

    same = set(
        o 
        for o in shared_keys 
        if d1[o] == d2[o]
    )

    return added, removed, modified, same

############################################
# Download a file from URL to location
# input: URL of ZWI
#        Location on the disk 
# Return: None
###########################################
existingDirs = {}

#############################################
def copyAuxilary(
    output: str,
    baseURL: str,
    verbose: bool = True
    ) -> None:
    """ Copy auxilary files to make the network visible """
    baseURL = baseURL.rstrip("/")

    path = output
    if (not path.endswith("/en") and not path.endswith("/en/")):
        path += "/en"

    urlpath = baseURL
    if (not urlpath.endswith("en") and not urlpath.endswith("en/")):
        urlpath += "/en"

    #print(output)
    #print(baseURL)
    ts = int(time.time())

    fileToCopy = {
        f"{urlpath}/index.html": [f"{path}/index.html",  ts],
        f"{baseURL}/index.txt":  [f"{output}/index.txt", ts],
        f"{baseURL}/sites.d":    [f"{output}/sites.d",   ts],
        f"{baseURL}/LICENSE":    [f"{output}/LICENSE",   ts]
    }

    getFileKeepTCP(
        fileToCopy,
        "",
        False
    ) # copy to local

    # get sites.d 
    getSites(baseURL)

    if verbose:
        print(f"Sites available: {', '.join(activeSites)}")

    # necessary site protection
    shutil.copyfile(f"{output}/index.txt", f"{output}/index.php")
    os.chmod(f"{path}/index.html", 755)
    os.chmod(f"{output}/en/index.html", 755)
    os.chmod(f"{output}/index.php", 755)
    os.system(f"chmod 755 {output}/*")

# Download a file from URL and place in the directory
# If the directory does not exist, make it
def getFile(
    xurl: str,
    filename: str
    ) -> bool:
    """  Download file from xurl to filename """

    try:
        r = session.get(
            xurl, 
            stream=True,
            timeout=(10, 300)
        )

    except requests.exceptions.RequestException as e:  # This is the correct syntax
        print(e)
        return False 

    if r.status_code == 200:
        # Set decode_content value to True, file's size will be zero.
        r.raw.decode_content = True

        # make directory if it is not here
        fullpath = os.path.dirname(filename)

        if fullpath in existingDirs.keys():
            with open(filename, 'wb') as f:
                shutil.copyfileobj(r.raw, f)

            return True 

        if not os.path.exists( fullpath ):
            os.makedirs( fullpath )
            existingDirs[fullpath] = True     

        else:
            existingDirs[fullpath] = True

        with open(filename,'wb') as f:
            shutil.copyfileobj(r.raw, f)

        return True 
    else:
        print(f"Error downloading {xurl}")
        print(f"Server returns the status: {r.status_code}")

        return False

# get sites.d to the main site index 
def getSites(
    xurl: str
    ) -> bool:
    ''' Download sites.d and add it to the main site index.
        Define URL for this site. 
        It returns true. 
    '''

    global activeSites

    if not xurl.endswith("sites.d"):
        xurl += "/sites.d"

    try:
        r = session.get(
            xurl,
            timeout=(10, 300)
        )
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        print(e)
        return False 

    if r.status_code == 200:
        data = r.text.split('\n') # then split it into lines

        for line in data:
            if line.endswith("ZWI"):
                line += "/"

            if line.endswith("ZWI/"):
                activeSites.append(line)

    activeSites = list(set(activeSites))
    return True 

# Download a file from URL and place in the directory
# Do not check directories. 
# Note: this is a slow method that restarts the TCP connection 
def getFileNoCheck(
    xurl: str,
    filename: str
    ) -> bool:

    # download file to location
    try:
        r = session.get(
            xurl,
            stream=True, 
            timeout=(10, 300)
        )
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        print(e)
        return False

    if r.status_code == 200:
        # Set decode_content value to True, file's size will be zero.
        r.raw.decode_content = True

        with open(filename, 'wb') as f:
            shutil.copyfileobj(r.raw, f)

        return True
    
    return False

def print_progress_bar(
    index: int, 
    total: int, 
    label: str
    ) -> None:
    """ draw progress bar """

    n_bar = 50  # Progress bar width
    progress = index / total

    # a bit easier to read
    bar =  f"[{'=' * int(n_bar * progress):{n_bar}s}]"
    bar += f"{int(100 * progress)}% {label}"

    sys.stdout.write('\r')
    sys.stdout.write(bar)
    sys.stdout.flush()

###############################################################################
# This method downloads a file from xurl to filename keeping the TCP connection
# alive. Note the xurl and filename are lists with URL and locations 
# Input: dictionary URL:output file
##############################################################################
def getFileKeepTCP(
    urlsAndfilenames: dict, 
    name: str = "",
    verb: bool = False
    ) -> None:
    """ download files using the same session. If name is given draw status bar.
        Input: dictionary dic[url]=[path,timestamp]
                name: name for this download publisher/publisher.org  
    """

    #print(urlsAndfilenames)
    total = len(urlsAndfilenames.items())
    if len(name) > 1: 
        verb = True
        fx = name.split("/")

        if len(fx) > 1:
            name = fx[0]

    n = 0
    
    # TODO:
    # Speedup!!
    for link, file in urlsAndfilenames.items():

        xfile, xtime = file

        res = getter(link, xfile)

        if not res:
            print(f"Error! Failed to download {link}!")
            continue

        else:
            os.utime(
                xfile, 
                (int(xtime), int(xtime))
            )

            if verb:
                n += 1

                print_progress_bar(
                    n, 
                    total, 
                    name
                )

    if verb:
        print(" ")

    #session.close() # close session

###############################################################################
# Create output directories from the file lists
# Input:  Global path for output 
#         List of remote ZWI files from index.d.gz
##############################################################################
def makeOutputDirectories(
    path: str,
    filelist: list
    ) -> None:
    """ Create output directories from the file list"""

    allDirs = {} # list of directories to be created
    for line in filelist:
        line = line.replace("\n", "")
        allDirs[os.path.dirname( line )] = 1

    # make directories 
    for key in allDirs.keys():
       xdir = f"{path}/{key}"
       if not os.path.exists(xdir):
          os.system(f"mkdir -p {path}/{key}")

def convert_size(
    size_bytes: int
    ) -> str:

    if size_bytes == 0:
        return "0B"

    #size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    size_name = ("MB")

    i = int(math.floor(math.log(
        size_bytes, 
        1024
    )))

    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)

    return f"{s} {size_name[i]}"

## print network summary
if args.summary:

    # first check if files already exist
    path = output
    if (not path.endswith("/en") and not path.endswith("/en/")):
        path = f"{output}/en"

    urlindex = root_repo
    if (not urlindex.endswith("/en") and not urlindex.endswith("/en/")):
        urlindex += "/en"

    # look at local directory
    if args.local:
        allDirs = {}
        allUrl = []  # URLs for wget

        print("[MSG] == ZWI directory summary == ")
        print(f"[MSG] : Local directory : {output}")
        files = glob.glob(
            f"{path}/**/*.zwi", 
            recursive=True
        )

        for line in files:
            allDirs[os.path.dirname( line )] = 1
            allUrl.append(line)

        print("[MSG] : Publishers:")

        nke=0
        ntotf=0
        print ("  {:<5} {:<10} {:<15}".format('Nr', 'Files', 'Publisher'))
        print ("  {:<5} {:<10} {:<15}".format('--', '-----', '---------'))

        for key in allDirs.keys():
            skey = key.replace(f"{output}/en/","")
            nke += 1
            vtot = 0

            for xli in allUrl:
                matches = re.findall(key, xli)
                vtot += len(matches)

            print ("  {:<5} {:<10} {:<15}".format(nke, vtot, skey))

        print("[MSG] : Total Nr of files =",len(allUrl))
        sys.exit()

    # now look at remote directories
    print("[MSG] == ZWI Network summary == ")
    print(f"[MSG] : Main repository: {root_repo}")
    globalIndex = getIndexRemote(
        urlindex,
        verbose=True
    )

    sys.exit()

###############################################
####### fresh download ######################## 
###############################################
def getIndexFromSiteMakeDirectories(
    output: str,
    url_site: str,
    excluded: list[str] = [],
    publishers: list[str] = [],
    verbose: bool = True
    ) -> bool | None:
    """ Download all indexes from the remote sizes and create the needed directories.
        It assumes that the disrectory does not exists.
        @return False if the directory ZWI/en exists 
    """  

    # first check if files already exist
    path = output
    if (not path.endswith("/en") and not path.endswith("/en/")):
        path = f"{output}/en"

    urlindex = url_site.rstrip("/")
    if (not url_site.endswith("/en") and not url_site.endswith("/en/")):
        urlindex = "/en"

    if not os.path.exists(path):
        os.makedirs(path)

        if verbose: 
           print(f"[MSG] Create: {path}")
           print(f"[MSG] Batch download to: {path}")

        #print(urlindex+"/index.csv.gz")
        stat = getFile(f"{urlindex}/index.csv.gz", f"{path}/index.csv.gz")
        if not stat:
            print("Cannot download index file!")
            sys.exit() 
    else:

        if not os.path.exists(f"{path}/index.csv.gz"):
           print(f"There is an error in downloading: {path}/index.csv.gz")
           sys.exit()

        err = False
        with gzip.open(f"{path}/index.csv.gz", "r") as fh:
            try:
                fh.read(1)
            except OSError:
                err = True
        
        if not err:
            if verbose:
                print(f"[MSG] path: {path} already exists")

            return err # returns false if previous installation is detected 

    mainIndex = {}

    indexFile = f"{path}/index.csv.gz"
    with gzip.open(indexFile,'rt') as f:
        for line in f:
            line = line.replace("\n","")
            lines = line.split("|")

            if len(lines) != 4:
                print(f"Problems in reading index file: {indexFile}")
                sys.exit()

            if len(excluded) > 0:
                remove = False
                for ex in excluded:

                    if lines[3].find(ex) > -1:
                        remove =True

                if remove:
                    continue

            if len(publishers) > 0:
               keep = False
               for ex in publishers:

                 if lines[3].find(ex) > -1:
                    keep = True 

               if not keep:
                continue

            mainIndex[lines[3]] = [
                lines[0],
                lines[1],
                lines[2]
            ]

            # print("TEST=",lines[3])

    urlsAndfilenames = {}
    # read data for specific publishers
    for key, data in mainIndex.items():
        pub_index = f"{key}.csv.gz"

        modtime = int(data[0]) # modification time from source 

        urlsAndfilenames[f"{urlindex}/{pub_index}"] = [f"{path}/{pub_index}", modtime] 
        # print("Read=",pub_index,modtime)
        
        ldir = f"{path}/{key}"
        if not os.path.exists(ldir):
            os.makedirs(ldir)

    print("getIndexFromSiteMakeDirectories:")
    getFileKeepTCP(
        urlsAndfilenames,
        "",
        False
    ) # download local index files to correct places 

    # download ZWI files 
    for key in mainIndex:
        pub_index = f"{path}/{key}.csv.gz"
        if not os.path.exists( pub_index ):
            print(f"There is an error in finding: {pub_index}")
            sys.exit()

        urlsAndfilenames = {}
        # download articles for a given publisher
        with gzip.open(pub_index,'rt') as f:
            for line in f:
                line = line.replace("\n","")
                lines = line.split("|")

                if len(lines) != 3:
                    print(f"Problems reading index file: {pub_index}")
                    return 

                modtime = int(lines[0])
                zwifile = f"{path}/{key}/{lines[2]}.zwi" 
                url = f"{urlindex}/{key}/{lines[2]}.zwi"

                url = urlSafe(url) # make URL safe for downloads 

                zwifile = zwifile.replace('"', '\"')
                urlsAndfilenames[url] = [zwifile,modtime] 

            #print()
            #print(pub_index)
            #print(urlsAndfilenames)

        print("getIndexFromSiteMakeDirectories 2:")
        getFileKeepTCP(
            urlsAndfilenames,
            key,
            True
        )

    return True

######################################################
############## update ZWI files ######################
######################################################
def pushUpdate(
    output: str,
    repourl: str,
    excluded: list[str],
    verbose: bool = True
    ) -> dict:
    """ update existing ZWI/en directory using the index files 
    output: str
            Output directory (ZWI/en)
    repourl: str
            URL from where update should be sourced 
    excluded: list
                List with excluded publishers 
    returns: dict{} 
            returns updated global index 
    """

    globalIndex = {} 
    outputdir = output.rstrip("/")
    if (not output.endswith("/en") and not output.endswith("/en/")):
        outputdir = f"{output}/en"

    if (not repourl.endswith("/en") and not repourl.endswith("/en/")):
        repourl += "/en"

    if verbose: 
        print_warn("## ZWI repository detected on local file system", "")
        print_warn(f"## Re-scan: {outputdir}", "")

    # currentIndex=createIndexFiles(outputdir, excluded, verbose) # get current index and write it to the directory 
    # print("Local summary=", shortSummaryOfIndex(currentIndex))
    currentIndex = getIndex(outputdir, verbose)     # read current local index file from file system 
    remoteIndex = getIndexRemote(repourl, verbose) # get remote index file 
    # print("Remote summary=", shortSummaryOfIndex(remoteIndex))

    if not currentIndex or not remoteIndex:
        print("Problems while getting index's")
        return {}

    # calculate delta of the current and online index files

    deltaList = getDeltaDifference(
        remoteIndex, 
        currentIndex
    )

    #print("Nr of Missing=",len(deltaList))

    if len(deltaList) > 0:

        # read data for specific publishers
        for key, listofzwi in deltaList.items():
            if len(key.strip()) < 1:
                continue

            if verbose:
                print(f"Update {len(deltaList[key])} files for {key}")

            ldir = f"{outputdir}/{key}"
            if not os.path.exists(ldir):
                os.makedirs(ldir)
                if verbose:
                    print(f"{ldir}does not exist. Make it!")

            urlsAndfilenames = {}

            if len(listofzwi) == 0:
                continue

            for f in listofzwi:
                fipath = f[0]
                fitime = int(f[1])

                zwi_site_url = f"{repourl}{key}/{fipath}.zwi"
                zwi_local_path = f"{outputdir}/{key}/{fipath}.zwi"
                zwi_site_url = urlSafe(zwi_site_url) # make URL safe for downloads 

                #zwi_local_path=zwi_local_path.replace('"', '\\"')
                urlsAndfilenames[zwi_site_url] = [zwi_local_path,fitime]
                #print(zwi_site_url, zwi_local_path)
            #print(urlsAndfilenames,key)

            getFileKeepTCP(
                urlsAndfilenames,
                key,
                False
            )

        globalIndex = createIndexFiles(
            outputdir, 
            excluded, 
            verbose
        ) # create new index  

    return globalIndex 

#######################################################
# Main part starts
#######################################################
# get all index files and create the needed directories
verbose = isVerbose

if __name__ == '__main__': 

    print_info("To stop press [Ctrl] + [C]", "")
    freshDownload = getIndexFromSiteMakeDirectories(
        output,
        root_repo,
        excluded,
        publishers,
        verbose
    )

    if (not freshDownload and not args.monitoring):
        print("Previous directory detected. Update!") 
        pushUpdate(
            output,
            root_repo,
            excluded,
            verbose
        ) # GET ZWI files for update 
        getSites(root_repo)                           # get sites.d

    ## copy auxilary files to make the network visible
    copyAuxilary(
        output, 
        root_repo, 
        verbose=True
    )

    if not args.monitoring: 
        print(f"Finish mirroring at {datetime.now()}")
        sys.exit() 

    ## monitoring loop
    nsites = f"1({str(len(activeSites))})"

    ################################# 
    # main monitoring loop with sleep
    #################################
    n = 0
    print("")
    print_info("Monitoring:  press [Ctrl] + [C] to stop", "")

    while True:
        time.sleep(60 * TimeDelayMin)

        xnow = datetime.now()
        current_site = activeSites[n]
        nsites = f"{n+1}({len(activeSites)})"

        if verbose:
            print(f"Current site: {current_site}")

        getSites(current_site)                   # get sites.d
        #if (freshDownload==False): freshDownload=getIndexFromSiteMakeDirectories(output,root_repo,verbose)
        globalIndexTMP = pushUpdate(
            output,
            current_site,
            excluded,
            verbose
        ) # GET ZWI files for update

        ctime = xnow.strftime('%Y-%m-%d %H:%M:%S')
        if len(globalIndexTMP) > 0: 
            xsum = shortSummaryOfIndex(globalIndexTMP)
            sss = f"{xsum[2]} articles, {xsum[0]} publishers"
            print(f"-> {ctime} {nsites} sites, {sss}")

        else:
            print(f"-> {ctime} {nsites} sites. No updates")
            n += 1

            if len(activeSites) == n:
                n = 0

    #print(f"Finish monitoring at: {datetime.now()}")